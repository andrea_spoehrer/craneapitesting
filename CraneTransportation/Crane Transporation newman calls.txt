-- QA CALL 
newman run QA_INT_Crane_Trans_APIs.postman_collection.json --environment CraneTransportation.postman_environment.json --reporters cli,junit --reporter-junit-export QA_Crane_Trans_Run_Results.xml --insecure --disable-unicode

-- TEST CALL
newman run TEST_EXT_Crane_Trans_APIs.postman_collection.json --environment CraneTransportation.postman_environment.json --reporters cli,junit --reporter-junit-export TEST_Crane_Trans_Run_Results.xml --insecure --disable-unicode

-- PROD CALL
newman run PROD_EXT_Crane_Trans_APIs.postman_collection.json --environment CraneTransportation.postman_environment.json --reporters cli,junit --reporter-junit-export PROD_Crane_Trans_Run_Results.xml --insecure --disable-unicode
